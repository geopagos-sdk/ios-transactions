#import <Foundation/Foundation.h>

//#define TLV EMVTLV2

@interface TLV : NSObject

@property (assign) int tag;
@property (copy) NSData *data;
@property (readonly) const unsigned char *bytes;
@property (readonly) bool constructed;

//for compatibility with old code
+(TLV *)tlvWithTag:(int)tag data:(NSData *)data;

+(TLV *)tlvWithString:(NSString *)data tag:(uint)tag;
+(TLV *)tlvWithHexString:(NSString *)data tag:(uint)tag;
+(TLV *)tlvWithData:(NSData *)data tag:(uint)tag;
+(TLV *)tlvWithInt:(UInt64)data nBytes:(int)nBytes tag:(uint)tag;
+(TLV *)tlvWithBCD:(UInt64)data nBytes:(int)nBytes tag:(uint)tag;

+(TLV *)findLastTag:(int)tag tags:(NSArray *)tags;
+(NSArray *)findTag:(int)tag tags:(NSArray *)tags;
+(NSArray *)decodeTags:(NSData *)data;
+(NSData *)encodeTags:(NSArray *)tags;
+(NSString *)hexEncodeTags:(NSArray *)tags;

@end
